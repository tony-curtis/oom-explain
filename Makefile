prefix ?= /lustre/software/oom-explain

INSTALL = install
INSTALL_BIN = $(prefix)/bin
INSTALL_MAN = $(prefix)/share/man/man1

.PHONY:	all	install	install-bin install-man clean

all:

install:	install-man	install-bin

install-bin:	oom-explain
	mkdir -p $(INSTALL_BIN)
	$(INSTALL) -m 755 $^ $(INSTALL_BIN)/$^

install-man:	oom-explain.1
	mkdir -p $(INSTALL_MAN)
	$(INSTALL) -m 644 $^ $(INSTALL_MAN)/$^

clean:
